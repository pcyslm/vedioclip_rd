# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 11:13:57 2018

@author: user009
DataVer  - Author - Note
20181030 - C.Y. Peng - First Release
"""
from moviepy.editor import VideoFileClip
from sys import path
path.append('../pydub/')
import subprocess
from pydub import AudioSegment
from pydub.silence import split_on_silence
import codecs
import gc

def main():
    VideoFileName = "Part3Old.mp3"
    ReadClipTimeFileName  = 'ClipTime.txt'
    #for _ in range(1000):
    #ClipFile = ReadVideoFile(VideoFileName)
        #del ClipFile
        #gc.collect()
    ClipTimeMatrix = ReadClipTimeFile(ReadClipTimeFileName)
    #print(__sec2secsec__(ClipTimeMatrix[0][1]))
    cmdVideoClip(VideoFileName, ClipTimeMatrix)
    #print(ClipTimeMatrix)
    #VideoClip(ClipFile, ClipTimeMatrix)

def cmdVideoClip(VideoFileName, ClipTimeMatrix):
    cmd_string = 'ffmpeg -i {tr} -acodec copy -ss {st} -to {en} {nm}.mp3'
    for idx in range(0, len(ClipTimeMatrix)):
        command = cmd_string.format(tr=VideoFileName, st=ClipTimeMatrix[idx][0], en=ClipTimeMatrix[idx][1], nm=str(idx))
        subprocess.call(command, shell=True)

def __sec2secsec__(time):
    thisTimeStr = time.split('.')
    return thisTimeStr[0]+":"+str(int(thisTimeStr[-1])*6)
    
def ReadVideoFile(VideoFileName):
    return AudioSegment.from_mp3(VideoFileName)

def ReadClipTimeFile(ReadClipTimeFileName):
    ClipTimeFile = codecs.open(ReadClipTimeFileName)
    Done  = bool(0)
    ClipTimeList = []
    while (not Done):
        LineInf = ClipTimeFile.readline().strip("\n")
        LineInfArray = LineInf.split('-')
        
        if (len(LineInfArray) == 1):
            Done = bool(1)
            ClipTimeFile.close()
        else:   
            ClipTimeList.append(LineInfArray)
            
    return ClipTimeList

def __min_sec2minseconds__(time):
    thisTimeStr = time.split(':') 
    return (int(thisTimeStr[0])*60+float(thisTimeStr[1]))*1000

def VideoClip(ClipFile, ClipTimeMatrix):
    #print(__min_sec2minseconds__(ClipTimeMatrix[0][0]))
    for idx in range(0, len(ClipTimeMatrix)):
        __VideoClip__(ClipFile, str(idx), __min_sec2minseconds__(ClipTimeMatrix[idx][0]), __min_sec2minseconds__(ClipTimeMatrix[idx][1]))
    
def __VideoClip__(ClipFile, WriteFileName, t_start, t_end):
    FinalClip = ClipFile[t_start:t_end]
    FinalClip.export(WriteFileName+'.mp3', format="mp3")

if __name__== "__main__":
    main()